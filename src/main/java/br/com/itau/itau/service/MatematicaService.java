package br.com.itau.itau.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class MatematicaService {

    public int somar(List<Integer> numeros) {
        int resultado = 0;
        for (int numero : numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public int multiplicar(List<Integer> numeros) {
        int resultado = 1;
        for (int numero : numeros) {
            resultado *= numero;
        }
        return resultado;
    }

    public int dividir(List<Integer> numeros) {
        Collections.sort(numeros);
        Collections.reverse(numeros);
        return (numeros.get(0).intValue())/(numeros.get(1).intValue());

    }

    public int subtrair(List<Integer> numeros) {
        Integer primeiroElemento = numeros.get(0);
        numeros.remove(0);
        int resultado = primeiroElemento;
        for (int numero : numeros) {
                resultado -= numero;
        }
        return resultado;
    }
}
