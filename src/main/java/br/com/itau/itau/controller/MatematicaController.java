package br.com.itau.itau.controller;

import br.com.itau.itau.model.Matematica;
import br.com.itau.itau.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        validarSeOsItensDaListaSaoNaturais(numeros);
        validarListaVaziaOuMenorQueDois(numeros);

        return matematicaService.somar(matematica.getNumeros());
    }

    @PostMapping("subtracao")
    public Integer subtracao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        validarSeOsItensDaListaSaoNaturais(numeros);
        validarListaVaziaOuMenorQueDois(numeros);

        return matematicaService.subtrair(matematica.getNumeros());
    }

    @PostMapping("divisao")
    public Integer divisao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        validarSeOsItensDaListaSaoNaturais(numeros);
        validarListaVaziaOuMenorQueDois(numeros);
        validarSeAListaEhMaiorQueDois(numeros);

        return matematicaService.dividir(matematica.getNumeros());
    }

    @PostMapping("multiplicacao")
    public Integer multiplicacao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        validarSeOsItensDaListaSaoNaturais(numeros);
        validarListaVaziaOuMenorQueDois(numeros);
        return matematicaService.multiplicar(matematica.getNumeros());
    }


    private void validarListaVaziaOuMenorQueDois(List<Integer> numeros) {
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "São necessarios pelo menos 2 números para esse calculo");
        }
    }

    private void validarSeAListaEhMaiorQueDois(List<Integer> numeros) {
        if (numeros.size() > 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Para a divisão, a lista não poderá ser maior que 2!");
        }
    }

    private void validarSeOsItensDaListaSaoNaturais(List<Integer> numeros) {
        for (Integer numero : numeros) {
            if (numero < 0 ) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Os itens da lista devem ser Numeros Naturais!");
            }
        }
    }


}
