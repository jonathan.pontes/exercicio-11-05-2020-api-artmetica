package br.com.itau.itau.model;

import java.util.List;

public class Matematica {

    public Matematica() {
    }

    private List<Integer> numeros;

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
